#!/bin/sh

git submodule update --init
git submodule foreach git checkout main
git submodule foreach npm i

# Entrypoints

cp ./entrypoints/entrypoint-front.sh ./front/entrypoint.sh
chmod +x ./front/entrypoint.sh

cp ./entrypoints/entrypoint-api.sh ./api/entrypoint.sh
chmod +x ./api/entrypoint.sh

# .env

#cp ./pierre-fabre-administration-api/.env.dist ./pierre-fabre-administration-api/.env
#sed -i 's/3000/3001/g' ./pierre-fabre-administration-api/.env
#sed -i 's/mongodb-api-administration/mongo/g' ./pierre-fabre-administration-api/.env