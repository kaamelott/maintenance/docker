# Docker-compose for MSPR Maintenance

Set up a fast dev environment (`api` and `front`) using docker.

## Prerequisite

You must have installed :
 - Node
 - Git
 - Docker

## Build your local environment

1. `sh build.sh`
- (linux) just run it like any shell script
- (windows) you can use Git Bash in the project directory to run it or use WSL2
2. `docker-compose up`

## Common issues fixes

- Are local react/mongo database ports are available ?
- Is docker running ?
- Permission error ? `chown user * -R`
